package world.urelion.autofollow;

import lombok.extern.slf4j.Slf4j;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * store of observing sessions
 *
 * @since 1.0.0
 */
@Slf4j
public class SessionRegistry {
	/**
	 * {@link Map}, which stores the observing sessions
	 *
	 * @since 1.0.0
	 */
	private final @NotNull Map<UUID, UUID>     sessions =
		new ConcurrentHashMap<>();
	/**
	 * {@link Map}, which stores the origin {@link GameMode} of an observer
	 *
	 * @since 1.1.0
	 */
	private final @NotNull Map<UUID, GameMode> gameModes =
		new ConcurrentHashMap<>();

	/**
	 * checks if a {@link Player} is an observer
	 *
	 * @param observer the {@link Player} to check
	 *
	 * @return {@code true} if the {@link Player} is an observer,
	 *         {@code false} otherwise
	 *
	 * @since 1.0.0
	 */
	public boolean isObserver(
		final @NotNull Player observer
	) {
		SessionRegistry.log.trace("Check for observer in sessions map.");
		return this.sessions.containsKey(observer.getUniqueId());
	}

	/**
	 * gives the observee of an observer
	 *
	 * @param observer the observer to get the observee of
	 *
	 * @return the observee of an observer
	 *
	 * @since 1.0.0
	 */
	public @Nullable Player getObservee(
		final @NotNull Player observer
	) {
		SessionRegistry.log.trace("Get observee UUID from sessions map.");
		final @Nullable UUID observeeId = this.sessions.get(
			observer.getUniqueId()
		);
		SessionRegistry.log.trace("Check if observee UUID exist.");
		if (observeeId == null) {
			SessionRegistry.log.error(
				"Observee UUID doesn't exist! Cancel lookup."
			);
			return null;
		}

		SessionRegistry.log.trace("Get observee from UUID.");
		return Bukkit.getPlayer(observeeId);
	}

	/**
	 * checks if a {@link Player} is an observee
	 *
	 * @param observee the {@link Player} to check
	 *
	 * @return {@code true} if the {@link Player} is an observee,
	 *         {@code false} otherwise
	 *
	 * @since 1.0.0
	 */
	public boolean isObservee(
		final @NotNull Player observee
	) {
		SessionRegistry.log.trace("Check for observee in sessions map.");
		return this.sessions.containsValue(observee.getUniqueId());
	}

	/**
	 * gives all observers of an observee
	 *
	 * @param observee the observee to get observers of
	 *
	 * @return a {@link Set} of all observers of the given observer
	 *
	 * @since 1.0.0
	 */
	public @NotNull Set<Player> getOberservers(
		final @NotNull Player observee
	) {
		SessionRegistry.log.trace(
			"Get set of observers, which follow the observee " +
			observee.getName() + "."
		);
		return this.sessions.entrySet().stream().filter(
			entry -> entry.getValue().equals(observee.getUniqueId())
		).map(
			entry -> Bukkit.getPlayer(entry.getKey())
		).collect(Collectors.toSet());
	}

	/**
	 * gives the amount of currently active sessions
	 *
	 * @return the amount of currently active sessions
	 *
	 * @since 1.0.0
	 */
	public int countSessions() {
		SessionRegistry.log.trace("Return amount of stored sessions.");
		return this.sessions.size();
	}

	/**
	 * create an observing session
	 *
	 * @param observer the observer of the new session
	 * @param observee the observee of the new session
	 *
	 * @return {@code true} if the session was successfully created,
	 *         {@code false} otherwise
	 *
	 * @since 1.0.0
	 */
	public boolean createSession(
		final @NotNull Player observer,
		final @NotNull Player observee
	) {
		SessionRegistry.log.trace("Get UUID of observee.");
		final @NotNull UUID observeeId = observee.getUniqueId();

		SessionRegistry.log.debug("Add sessions to managed map.");
		this.sessions.put(observer.getUniqueId(), observeeId);

		SessionRegistry.log.trace("Get stored observee.");
		final @Nullable Player storedObservee = this.getObservee(observer);
		SessionRegistry.log.trace("Check if stored observee exist.");
		if (storedObservee == null) {
			SessionRegistry.log.error(
				"Observee couldn't stored! Return failed status."
			);
			return false;
		}
		SessionRegistry.log.trace("Check if session was successfully added.");
		return storedObservee.getUniqueId().equals(observeeId);
	}

	/**
	 * closes a session by its observer
	 *
	 * @param observer the observer of the session to close
	 *
	 * @return the observee of the closed session
	 *
	 * @since 1.0.0
	 */
	public @Nullable Player closeObserverSession(
		final @NotNull Player observer
	) {
		SessionRegistry.log.trace("Remove session from managed map.");
		final @Nullable Player observee = Bukkit.getPlayer(this.sessions.remove(
			observer.getUniqueId()
		));
		SessionRegistry.log.trace(
			"Check if observer is currently in spectator mode."
		);
		if (observer.getGameMode() == GameMode.SPECTATOR) {
			SessionRegistry.log.trace("Get game mode of observer.");
			final @Nullable GameMode gameMode = this.getGameMode(observer);
			SessionRegistry.log.trace("Check if game mode is set.");
			if (gameMode != null) {
				SessionRegistry.log.debug(
					"Set game mode back to origin of observer " +
					observer.getName()
				);
				observer.setGameMode(gameMode);
			}
		}
		SessionRegistry.log.debug("Close observer session.");
		this.resetGameMode(observer);
		SessionRegistry.log.trace(
			"Send message to observer about closed session."
		);
		observer.sendMessage("You will not longer following " + (
			observee != null ? observee.getName() : "another player"
		) + ".");
		SessionRegistry.log.trace(
			"Exit command execution with successful result."
		);

		SessionRegistry.log.trace("Return observee.");
		return observee;
	}

	/**
	 * closes all sessions by its observee
	 *
	 * @param observee the observee of the session to close
	 *
	 * @return a {@link Set} of all observers of the closed sessions
	 *
	 * @since 1.0.0
	 */
	public @NotNull Set<Player> closeObserveeSessions(
		final @NotNull Player observee
	) {
		SessionRegistry.log.trace("Get sessions by observee.");
		final @NotNull Set<Player> observers = this.getOberservers(observee);

		SessionRegistry.log.trace("Iterate over all found sessions.");
		for (final @Nullable Player observer : observers) {
			SessionRegistry.log.trace("Check if observer exist.");
			if (observer == null) {
				SessionRegistry.log.error(
					"Observer doesn't exist! Continue with next observer."
				);
				continue;
			}

			SessionRegistry.log.trace("Remove session from managed map.");
			this.closeObserverSession(observer);
		}

		SessionRegistry.log.trace(
			"Return set of observers of removed sessions."
		);
		return observers;
	}

	/**
	 * gives the origin {@link GameMode} of an observer
	 *
	 * @param observer the {@link Player} to get the {@link GameMode} of
	 *
	 * @return the origin {@link GameMode} of the given observer
	 *
	 * @since 1.1.0
	 */
	public @Nullable GameMode getGameMode(
		final @NotNull Player observer
	) {
		SessionRegistry.log.trace(
			"Return the stored game mode of observer " + observer.getName()
		);
		return this.gameModes.get(observer.getUniqueId());
	}

	/**
	 * sets the origin {@link GameMode} of an observer
	 *
	 * @param observer the {@link Player}
	 *                 to store the origin {@link GameMode} of
	 * @param gameMode the origin {@link GameMode} to store
	 *
	 * @return {@code true} if the {@link GameMode} is successfully stored,
	 *         {@code false} otherwise
	 *
	 * @since 1.1.0
	 */
	public boolean setGameMode(
		final @NotNull Player observer,
		final @NotNull GameMode gameMode
	) {
		SessionRegistry.log.debug(
			"Store the origin game mode " + gameMode +
			" of observer " + observer.getName()
		);
		this.gameModes.put(observer.getUniqueId(), gameMode);
		SessionRegistry.log.trace(
			"Check if origin game mode is successfully stored."
		);
		return this.getGameMode(observer) == gameMode;
	}

	/**
	 * resets an origin {@link GameMode}
	 *
	 * @param observer the {@link Player} to reset the {@link GameMode} of
	 *
	 * @return {@code true} if the {@link GameMode} is successfully reset,
	 *         {@code false} otherwise
	 *
	 * @since 1.1.0
	 */
	public boolean resetGameMode(
		final @NotNull Player observer
	) {
		SessionRegistry.log.trace("Get the UUID of the observer.");
		final @NotNull UUID observerUuid = observer.getUniqueId();

		SessionRegistry.log.debug(
			"Clear stored game mode of observer " + observer.getName()
		);
		this.gameModes.remove(observerUuid);
		SessionRegistry.log.trace(
			"Check if origin game mode is successfully cleared."
		);
		return !(this.gameModes.containsKey(observerUuid));
	}
}
