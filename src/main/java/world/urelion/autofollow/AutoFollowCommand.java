package world.urelion.autofollow;

import lombok.extern.slf4j.Slf4j;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link CommandExecutor} for {@link Command} <i>/autofollow</i>
 *
 * @since 1.0.0
 */
@Slf4j
public class AutoFollowCommand
implements TabExecutor {
	/**
	 * executes {@link Command} <i>/autofollow</i>
	 *
	 * @param sender  the {@link CommandExecutor},
	 *                which triggers the {@link Command}
	 * @param command the {@link Command} to execute
	 * @param label   the {@link Command} alias, which is used
	 * @param args    the {@link Command} arguments
	 *
	 * @return {@code true} if the {@link Command} was used in the right way,
	 *         {@code false} otherwise
	 *
	 * @since 1.0.0
	 */
	@Override
	public boolean onCommand(
		final @NotNull CommandSender sender,
		final @NotNull Command command,
		final @NotNull String label,
		final @NotNull String[] args
	) {
		AutoFollowCommand.log.trace("Check if command sender is a player.");
		if (!(sender instanceof Player)) {
			AutoFollowCommand.log.warn(
				"Command sender is not a player!" +
				" Send error message to command sender."
			);
			sender.sendMessage(
				"Only an online player could execute this command!"
			);
			AutoFollowCommand.log.trace(
				"Exit command execution with successful result."
			);
			return true;
		}

		AutoFollowCommand.log.trace("Cast command sender as player.");
		final @NotNull Player observer = (Player)sender;

		AutoFollowCommand.log.trace("Get plugin instance.");
		final @Nullable AutoFollowPlugin autoFollowPlugin =
			AutoFollowPlugin.getINSTANCE();
		AutoFollowCommand.log.trace("Check if plugin is initialized.");
		if (autoFollowPlugin == null) {
			AutoFollowCommand.log.error(
				"Plugin not initialized! Send error message to observer."
			);
			observer.sendMessage(
				"The plugin ${plugin.name} is not initialized!"
			);
			return true;
		}

		AutoFollowCommand.log.trace("Get permission provider.");
		final @Nullable Permission permission =
			autoFollowPlugin.getPermissionProvider();
		AutoFollowCommand.log.trace(
			"Check if permission provider is initialized."
		);
		if (permission == null) {
			AutoFollowCommand.log.error(
				"Permission provider is not initialized! " +
				"Send error message to observer."
			);
			observer.sendMessage("Can't check permissions!");
			AutoFollowCommand.log.trace(
				"Exit command execution with failed result."
			);
			return true;
		}

		AutoFollowCommand.log.trace("Get name of observer.");
		final @NotNull String observerName = observer.getName();

		AutoFollowCommand.log.trace(
			"Check if observer has permission to execute this command."
		);
		if (!(permission.has(
			observer,
			"${permission.namespace.plugin}.command"
		))) {
			AutoFollowCommand.log.debug(
				"Observer " + observerName +
				" has no permission to execute this command!" +
				" Send error message to observer."
			);
			observer.sendMessage("You are not allowed to use this command!");
			AutoFollowCommand.log.trace(
				"Exit command execution with successful result."
			);
			return true;
		}

		AutoFollowCommand.log.trace(
			"Check if observer is allowed to observe other players."
		);
		if (!(permission.has(
			observer,
			"${permission.namespace.plugin}.observer"
		))) {
			AutoFollowCommand.log.debug(
				"Observer " + observerName +
				" is not allowed to follow other players!" +
				" Send error message to observer."
			);
			observer.sendMessage("You are not allowed to follow other players!");
			AutoFollowCommand.log.trace(
				"Exit command execution with successful result."
			);
			return true;
		}

		AutoFollowCommand.log.trace("Get amount of arguments.");
		final int argsLength = args.length;

		AutoFollowCommand.log.trace("Check if sub-command is set.");
		if (argsLength < 1) {
			AutoFollowCommand.log.debug(
				"No sub-command given! Send error message to observer."
			);
			observer.sendMessage("You have to set a sub-command.");
			AutoFollowCommand.log.trace(
				"Exit command execution with failed result."
			);
			return false;
		}

		AutoFollowCommand.log.trace("Get plugins sessions registry.");
		final @NotNull SessionRegistry sessionRegistry =
			autoFollowPlugin.getSessionRegistry();

		AutoFollowCommand.log.trace("Get sub-command from arguments.");
		final @NotNull String subCommand = args[0];

		AutoFollowCommand.log.trace("Select sub-command.");
		if ("stop".equalsIgnoreCase(subCommand)) {
			AutoFollowCommand.log.trace("Close session of " + observerName);
			sessionRegistry.closeObserverSession(observer);
			AutoFollowCommand.log.trace(
				"Exit command execution with successful result."
			);
			return true;
		} else if ("start".equalsIgnoreCase(subCommand)) {
			AutoFollowCommand.log.trace("Check if observee is set in command.");
			if (argsLength < 2) {
				AutoFollowCommand.log.debug(
					"Observee is missing in command!" +
					" Send error message to observer."
				);
				observer.sendMessage("The player to follow is missing!");
				AutoFollowCommand.log.trace(
					"Exit command execution with failed result."
				);
				return false;
			}

			AutoFollowCommand.log.trace(
				"Get name of observee from command arguments."
			);
			final @Nullable String observeeName = args[1];
			AutoFollowCommand.log.trace("Get observee player.");
			final @Nullable Player observee = Bukkit.getPlayer(observeeName);

			AutoFollowCommand.log.trace("Check if observee exist.");
			if (observee == null) {
				AutoFollowCommand.log.debug(
					"No player found with observee name: " + observeeName +
					"! Send error message to observer."
				);
				observer.sendMessage(
					"Player " + observeeName + " couldn't be found!"
				);
				AutoFollowCommand.log.trace(
					"Exit command execution with successful result."
				);
				return true;
			}

			AutoFollowCommand.log.trace(
				"Check if it is allowed to follow the observee."
			);
			if (!(permission.has(
				observee,
				"${permission.namespace.plugin}.observee"
			))) {
				AutoFollowCommand.log.debug(
					"It is not allowed to follow the player " + observeeName +
					"! Send error message to observer."
				);
				observer.sendMessage(
					"It is not allowed to follow " + observeeName + "!"
				);
				AutoFollowCommand.log.trace(
					"Exit command execution with successful result."
				);
				return true;
			}

			AutoFollowCommand.log.trace("Check if observee is online.");
			if (!(observee.isOnline())) {
				AutoFollowCommand.log.debug(
					"Observee is not offline! Send error message to observer."
				);
				observer.sendMessage(
					observeeName +
					" is offline. Please select an online player to follow."
				);
				AutoFollowCommand.log.trace(
					"Exit command execution with successful result."
				);
				return true;
			}

			AutoFollowCommand.log.trace(
				"Check if observer and observee are identical."
			);
			if (
				observer.equals(observee) ||
				observer.getUniqueId().equals(observee.getUniqueId())
			) {
				AutoFollowCommand.log.debug(
					"Observer and observee are identical!" +
					" Send error message to observer."
				);
				observer.sendMessage("You can't follow yourself!");
				AutoFollowCommand.log.trace(
					"Exit command execution with successful result."
				);
				return true;
			}

			AutoFollowCommand.log.trace("Create observing session.");
			final boolean sessionAdded =
				autoFollowPlugin.getSessionRegistry().createSession(
					observer,
					observee
				);
			AutoFollowCommand.log.trace(
				"Check if session was successfully created."
			);
			if (sessionAdded) {
				AutoFollowCommand.log.trace(
					"Set origin game mode of observer " + observerName
				);
				sessionRegistry.setGameMode(observer, observer.getGameMode());
				AutoFollowCommand.log.debug(
					"Set observer " + observerName + " to spectator mode."
				);
				observer.setGameMode(GameMode.SPECTATOR);
				AutoFollowCommand.log.debug("Teleport observer to observee.");
				observer.teleport(observee.getLocation());
				AutoFollowCommand.log.trace(
					"Send message to observer about session start."
				);
				observer.sendMessage(
					"You will now following " + observeeName + " automatically."
				);
			} else {
				AutoFollowCommand.log.error(
					"Session couldn't created! Send error message to observer."
				);
				observer.sendMessage("An error occurred on start following.");
			}
			AutoFollowCommand.log.trace(
				"Return if session was successfully added."
			);
			return sessionAdded;
		} else {
			AutoFollowCommand.log.debug(
				"Unknown sub-command given! Send error message to observer."
			);
			observer.sendMessage("Unknown sub-command: " + subCommand);
			AutoFollowCommand.log.trace(
				"Exit command execution with failed result."
			);
			return false;
		}
	}

	/**
	 * gives suggestions of {@link Command} arguments
	 *
	 * @param sender  the {@link CommandSender},
	 *                which triggers the tab completion
	 * @param command the {@link Command} to get the arguments of
	 * @param label   the {@link Command} alias, which is used
	 * @param args    the {@link Command} arguments
	 *
	 * @return a {@link List} of all possible suggestions
	 *
	 * @since 1.0.0
	 */
	@Override
	public @NotNull List<String> onTabComplete(
		final @NotNull CommandSender sender,
		final @NotNull Command command,
		final @NotNull String label,
		final @NotNull String[] args
	) {
		AutoFollowCommand.log.trace("Create list of suggestions.");
		final @NotNull List<String> suggestions = new ArrayList<>();

		AutoFollowCommand.log.trace(
			"Check, which argument position should suggested."
		);
		if (args.length < 2) {
			AutoFollowCommand.log.trace(
				"Add 'start' sub-command to suggestions."
			);
			suggestions.add("start");
			AutoFollowCommand.log.trace(
				"Add 'stop' sub-command to suggestions."
			);
			suggestions.add("stop");
			AutoFollowCommand.log.trace("Return list of first suggestions.");
			return suggestions;
		}

		AutoFollowCommand.log.trace("Get plugins instance.");
		final @Nullable AutoFollowPlugin autoFollowPlugin =
			AutoFollowPlugin.getINSTANCE();
		AutoFollowCommand.log.trace("Check, if plugin was initialized.");
		if (autoFollowPlugin == null) {
			AutoFollowCommand.log.error(
				"Plugin not initialized! Cancel tab completion."
			);
			return suggestions;
		}

		AutoFollowCommand.log.trace("Get senders name.");
		final @NotNull String senderName = sender.getName();

		AutoFollowCommand.log.trace("Iterate over all online players.");
		for (
			final @Nullable Player player :
			autoFollowPlugin.getServer().getOnlinePlayers()
		) {
			AutoFollowCommand.log.trace("Check if online player exist.");
			if (player == null) {
				AutoFollowCommand.log.warn(
					"Online player doesn't exist!" +
					" Continue with next online player."
				);
				continue;
			}

			AutoFollowCommand.log.trace("Get name of online player.");
			final @NotNull String playerName = player.getName();

			AutoFollowCommand.log.trace(
				"Check is online player is identical with command sender."
			);
			if (playerName.equalsIgnoreCase(senderName)) {
				AutoFollowCommand.log.trace(
					"Online player is identical with command sender." +
					" Continue with next online player."
				);
				continue;
			}

			AutoFollowCommand.log.trace("Add online player to suggestions.");
			suggestions.add(playerName);
		}

		AutoFollowCommand.log.trace("Return list of second suggestions.");
		return suggestions;
	}
}
