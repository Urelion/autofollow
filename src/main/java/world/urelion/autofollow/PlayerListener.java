package world.urelion.autofollow;

import lombok.extern.slf4j.Slf4j;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * {@link Listener} to reacts on {@link PlayerEvent}s
 *
 * @since 1.0.0
 */
@Slf4j
public class PlayerListener
implements Listener {
	/**
	 * process if a {@link Player} moves
	 *
	 * @param event the {@link PlayerMoveEvent} to process
	 *
	 * @since 1.0.0
	 */
	@EventHandler
	public void onPlayerMove(final PlayerMoveEvent event) {
		PlayerListener.log.trace("Check if event exist.");
		if (event == null) {
			PlayerListener.log.error(
				"Event doesn't exist! Cancel event handling."
			);
			return;
		}

		PlayerListener.log.trace("Get target location.");
		final @Nullable Location to = event.getTo();
		PlayerListener.log.trace("Check if target location exist.");
		if (to == null) {
			PlayerListener.log.warn(
				"Target location doesn't exist! Cancel event handling."
			);
			return;
		}

		PlayerListener.log.trace("Get plugin instance.");
		final @Nullable AutoFollowPlugin autoFollowPlugin =
			AutoFollowPlugin.getINSTANCE();
		PlayerListener.log.trace("Check if plugin is not initialized.");
		if (autoFollowPlugin == null) {
			PlayerListener.log.error(
				"Plugin not initialized! Cancel event handling."
			);
			return;
		}

		PlayerListener.log.trace("Get permissions provider.");
		final @Nullable Permission permission =
			autoFollowPlugin.getPermissionProvider();
		PlayerListener.log.trace(
			"Check if permission provider is initialized."
		);
		if (permission == null) {
			PlayerListener.log.error(
				"Permission provider is not initialized! Cancel event handling."
			);
			return;
		}

		PlayerListener.log.trace("Get observee from event.");
		final @NotNull Player observee = event.getPlayer();

		PlayerListener.log.trace("Get plugins sessions registry.");
		final @NotNull SessionRegistry sessionRegistry =
			autoFollowPlugin.getSessionRegistry();

		PlayerListener.log.trace(
			"Check if it is allowed to follow the observee."
		);
		if (!(permission.has(
			observee,
			"${permission.namespace.plugin}.observee"
		))) {
			PlayerListener.log.debug(
				"It is not longer allowed to follow the player " +
				observee.getName() + "! Close all sessions of this observee."
			);
			sessionRegistry.closeObserveeSessions(observee);
			PlayerListener.log.trace(
				"Cancel event handling due no open sessions with this observee."
			);
			return;
		}

		PlayerListener.log.trace(
			"Iterate over all observers, which follows the player."
		);
		for (final @Nullable Player observer : sessionRegistry.getOberservers(
			observee
		)) {
			PlayerListener.log.trace("Check if observer exist.");
			if (observer == null) {
				PlayerListener.log.warn(
					"Observer doesn't exist! Continue with next observer."
				);
				continue;
			}

			PlayerListener.log.trace(
				"Check if observer is allowed to follow other players."
			);
			if (!(permission.has(
				observer,
				"${permission.namespace.plugin}.observer"
			))) {
				PlayerListener.log.debug(
					"Observer " + observer.getName() +
					" is no longer allowed to follow other players!" +
					" Close all sessions of this observee."
				);
				sessionRegistry.closeObserverSession(observer);
				PlayerListener.log.trace("Continue with next observer.");
				continue;
			}

			PlayerListener.log.trace("Teleport observer to target location.");
			observer.teleport(to);
		}
	}

	/**
	 * process if {@link Player} quits the server
	 *
	 * @param event the {@link PlayerQuitEvent} to process
	 *
	 * @since 1.0.0
	 */
	@EventHandler
	public void onPlayerQuit(final PlayerQuitEvent event) {
		PlayerListener.log.trace("Call player disconnection handling.");
		this.onPlayerDisconnect(event);
	}

	/**
	 * process if {@link Player} was kicked from the server
	 *
	 * @param event the {@link PlayerKickEvent} to process
	 *
	 * @since 1.0.0
	 */
	@EventHandler
	public void onPlayerKick(final PlayerKickEvent event) {
		PlayerListener.log.trace("Call player disconnection handling.");
		this.onPlayerDisconnect(event);
	}

	/**
	 * process if {@link Player} disconnects from the server
	 *
	 * @param event the {@link PlayerEvent} to process
	 *
	 * @since 1.0.0
	 */
	private void onPlayerDisconnect(
		final @Nullable PlayerEvent event
	) {
		PlayerListener.log.trace("Check if event exist.");
		if (event == null) {
			PlayerListener.log.error(
				"Event doesn't exist! Cancel event handling."
			);
			return;
		}

		PlayerListener.log.trace("Get plugin instance.");
		final @Nullable AutoFollowPlugin autoFollowPlugin =
			AutoFollowPlugin.getINSTANCE();
		PlayerListener.log.trace("Check if plugin is initialized.");
		if (autoFollowPlugin == null) {
			PlayerListener.log.error(
				"Plugin is not initialized! Cancel event handling."
			);
			return;
		}

		PlayerListener.log.trace("Get session registry.");
		final @NotNull SessionRegistry sessionRegistry =
			autoFollowPlugin.getSessionRegistry();

		PlayerListener.log.trace("Get player from event.");
		final @NotNull Player player = event.getPlayer();

		PlayerListener.log.trace("Get players name.");
		final @NotNull String playerName = player.getName();

		PlayerListener.log.trace(
			"Remove session, where " + playerName + " is observer."
		);
		sessionRegistry.closeObserverSession(player);
		PlayerListener.log.trace(
			"Remove sessions, where " + playerName + " is observee."
		);
		sessionRegistry.closeObserveeSessions(player);
	}
}
